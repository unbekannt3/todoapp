import { Pipe, PipeTransform } from '@angular/core';
import { Todo } from '../models/todo';

@Pipe({
  name: 'todoFilter'
})
export class TodoFilterPipe implements PipeTransform {

  transform(todos: Todo[], suchtext: string): Todo[] {

    if (!suchtext) {
      return todos;
    }
    todos = todos.filter(t =>
      t.label.toUpperCase().indexOf(suchtext.toUpperCase()) >= 0
      || String(t.id) === suchtext
      || String(t.category) === suchtext
      || String(t.priority) === suchtext
      || String(t.dueDate) === suchtext
    );
    return todos;
  }

}
