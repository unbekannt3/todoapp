import { Component, OnInit } from '@angular/core';
import { TodoDataService } from '../../services/todo-data.service';

@Component({
  selector: 'app-todo-statistics',
  templateUrl: './todo-statistics.component.html',
  styleUrls: ['./todo-statistics.component.scss']
})
export class TodoStatisticsComponent implements OnInit {

  constructor(private todoDataService: TodoDataService) { }

  get todosLength(): number {
    return this.todoDataService.todoList.length;
  }

  get checkedTodos(): number {

    return this.todoDataService.todoList.filter(t => t.done !== false).length;
  }

  ngOnInit() {
  }

}
