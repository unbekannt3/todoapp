import { Component, OnInit } from '@angular/core';

import { SettingsService } from '../../services/settings.service';
import { TodoDataService } from '../../services/todo-data.service';
import { Title } from '@angular/platform-browser';

declare var swal: any;

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {

  constructor(
    private todoDataService: TodoDataService,
    private settingsService: SettingsService,
    title: Title
  ) {

    title.setTitle('Einstellungen - TodoApp');

  }

  // Toggle Delete
  toggleDelete() {
    this.settingsService.toggleDeleteButton();
  }

  // Toggle Search Field
  toggleSearchField() {
    this.settingsService.toggleSearchField();
  }

  // Toggle Todo Ordering
  toggleTodoOrdering() {
    this.settingsService.toggleTodoOrdering();
  }

  // Load Example Todos
  loadExampleData() {

    this.settingsService.loadExampleData();
  }

  // Remove all Todos
  deleteAll() {
    this.settingsService.deleteAll(true);
  }

  get todosLeer(): boolean {
    return this.todoDataService.todosLeer;
  }

  get delEnabled() {

    return this.settingsService.delEnabled;
  }

  get searchEnabled() {

    return this.settingsService.searchEnabled;
  }

  get todoOrderingEnabled() {

    return this.settingsService.listSortEnabled;
  }

  get getVersion() {

    return this.settingsService.appVersion;
  }

  ngOnInit() {
  }

}
