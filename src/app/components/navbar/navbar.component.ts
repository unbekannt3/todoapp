import { Title } from '@angular/platform-browser';
import { Component, OnInit } from '@angular/core';
import { TodoListComponent } from '../todo-list/todo-list.component';
import { TodoFormComponent } from '../todo-form/todo-form.component';
import { Routes } from '@angular/router';
import { SettingsService } from '../../services/settings.service';
import { TodoDataService } from '../../services/todo-data.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  public constructor(
    private titleService: Title,
    private todoDataService: TodoDataService,
  ) { }

  setTitle(newTitle: string) {
    this.titleService.setTitle(newTitle);
  }

  ngOnInit() {
  }

}
