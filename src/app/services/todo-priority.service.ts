import { Injectable } from '@angular/core';
import { Priority } from '../models/priority';

@Injectable()
export class TodoPriorityService {

  public priorities: Priority[] = [];

  constructor() {

    this.priorities.push(new Priority(1, 'Normal'));
    this.priorities.push(new Priority(2, 'Wichtig'));
    this.priorities.push(new Priority(3, 'Kritisch'));

  }

  // adds a new priority
  add(_priority: Priority) {

    this.priorities.push(new Priority(this.priorities.length + 1, _priority.name));

  }

}
