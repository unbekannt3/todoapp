import { TestBed, inject } from '@angular/core/testing';

import { TodoPriorityService } from './todo-priority.service';

describe('TodoPriorityService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TodoPriorityService]
    });
  });

  it('should be created', inject([TodoPriorityService], (service: TodoPriorityService) => {
    expect(service).toBeTruthy();
  }));
});
