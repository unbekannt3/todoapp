import { BrowserModule } from '@angular/platform-browser';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { APP_BASE_HREF } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { RouterModule, Routes } from '@angular/router';
import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';
import { MyDatePickerModule } from 'mydatepicker/dist';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AlertModule } from 'ngx-alerts';

import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { PriorityChooserComponent } from './components/priority-chooser/priority-chooser.component';
import { SettingsComponent } from './components/settings/settings.component';
import { TodoFormComponent } from './components/todo-form/todo-form.component';
import { TodoListComponent } from './components/todo-list/todo-list.component';
import { TodoStatisticsComponent } from './components/todo-statistics/todo-statistics.component';
import { TodoFilterPipe } from './pipes/todo-filter.pipe';
import { TodoListOrderingPipe } from './pipes/todo-list-ordering.pipe';
import { TodoDataService } from './services/todo-data.service';
import { TodoCategoryService } from './services/todo-category.service';
import { TodoPriorityService } from './services/todo-priority.service';
import { MessageBoxService } from './services/message-box.service';
import { SettingsService } from './services/settings.service';

const appRoutes: Routes = [
  {
    path: 'todos',
    component: TodoListComponent,
    data: { title: '' }
  },
  {
    path: 'add',
    component: TodoFormComponent,
    data: { title: 'Neues Todo' }
  },
  {
    path: 'settings',
    component: SettingsComponent,
    data: { title: 'Einstellungen' }
  },
  {
    path: '',
    redirectTo: '/todos',
    pathMatch: 'full'
  }
];


@NgModule({
  declarations: [
    AppComponent,
    TodoListComponent,
    TodoFormComponent,
    TodoStatisticsComponent,
    NavbarComponent,
    TodoFilterPipe,
    SettingsComponent,
    TodoListOrderingPipe,
    PriorityChooserComponent
  ],
  imports: [
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: false } // <-- debugging purposes only
    ),
    SweetAlert2Module.forRoot(),
    BrowserModule,
    MDBBootstrapModule.forRoot(),
    FormsModule,
    MyDatePickerModule,
    HttpClientModule,
    BrowserAnimationsModule, // required animations module
    AlertModule.forRoot({ maxMessages: 5, timeout: 5000, position: 'right' })
  ],
  providers: [
    TodoDataService,
    TodoCategoryService,
    TodoPriorityService,
    MessageBoxService,
    SettingsService
  ],
  bootstrap: [AppComponent],
  schemas: [NO_ERRORS_SCHEMA]
})
export class AppModule { }
