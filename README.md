# Angular TodoApp

[![pipeline status](https://gitlab.com/unbekannt3/todoapp/badges/master/pipeline.svg)](https://gitlab.com/unbekannt3/todoapp/commits/master)

Small Angular TodoApp with use of localStorage API for storing settings and todos in your Browser.

You can use this project in the current master state with Angular 8 on my [School Projects](https://school.unbekannt3.eu/TodoApp/) site  
**OR** in an old version back from 2017 with JQuery Bootstrap 3 and Angular 4 [here](https://school.unbekannt3.eu/TodoAppOLD/).

## Libraries used

 - [MDBootstrap](https://mdbootstrap.com/docs/angular/i)
 - [FontAwesome 5](https://fontawesome.com/)
 - [SweetAlert 2](https://sweetalert2.github.io/)
 - [ngx-alerts](https://www.npmjs.com/package/ngx-alerts)

## Development server

Run `ng serve` or `npm run start` for a local dev server. Navigate to `http://localhost:4200/` with a modern Browser like Chrome or Firefox. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build which can be uploaded to a production webserver.
